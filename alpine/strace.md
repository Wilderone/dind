# Билд основного контейнера:
=================================================
root@vm1:/home/max/dind# docker build .
Sending build context to Docker daemon  3.072kB
Step 1/8 : FROM alpine:latest
 ---> 63081882c6ff
Step 2/8 : RUN apk add --no-cache docker strace
 ---> Running in 63c6db96541b
fetch http://dl-cdn.alpinelinux.org/alpine/v3.12/main/x86/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.12/community/x86/APKINDEX.tar.gz
(1/13) Installing ca-certificates (20191127-r4)
(2/13) Installing libseccomp (2.4.3-r0)
(3/13) Installing runc (1.0.0_rc10-r1)
(4/13) Installing containerd (1.3.4-r1)
(5/13) Installing libmnl (1.0.4-r0)
(6/13) Installing libnftnl-libs (1.1.6-r0)
(7/13) Installing iptables (1.8.4-r1)
(8/13) Installing tini-static (0.19.0-r0)
(9/13) Installing device-mapper-libs (2.02.186-r1)
(10/13) Installing docker-engine (19.03.12-r0)
(11/13) Installing docker-cli (19.03.12-r0)
(12/13) Installing docker (19.03.12-r0)
Executing docker-19.03.12-r0.pre-install
(13/13) Installing strace (5.6-r0)
Executing busybox-1.31.1-r16.trigger
Executing ca-certificates-20191127-r4.trigger
OK: 213 MiB in 27 packages
Removing intermediate container 63c6db96541b
 ---> 4ab7ccc61f2d
Step 3/8 : RUN mkdir /dock
 ---> Running in 081794bd9cde
Removing intermediate container 081794bd9cde
 ---> d37f48acc63f
Step 4/8 : RUN touch /dock/Dockerfile
 ---> Running in 2887817f7d81
Removing intermediate container 2887817f7d81
 ---> bf425ca373d5
Step 5/8 : RUN strace ls
 ---> Running in b29b08832aaf
execve("/bin/ls", ["ls"], 0xbfb461c0 /* 5 vars */) = 0
set_thread_area({entry_number=-1, base_addr=0xb7f38e64, limit=0x0fffff, seg_32bit=1, contents=0, read_exec_only=0, limit_in_pages=1, seg_not_present=0, useable=1}) = 0 (entry_number=6)
set_tid_address(0xb7f39348)             = 8
mprotect(0xb7f36000, 4096, PROT_READ)   = 0
mprotect(0x506000, 8192, PROT_READ)     = 0
getuid32()                              = 0
clock_gettime(CLOCK_REALTIME, {tv_sec=1595819762, tv_nsec=969929297}) = 0
ioctl(0, TIOCGWINSZ, 0xbfdb1a80)        = -1 ENOTTY (Not a tty)
ioctl(1, TIOCGWINSZ, 0xbfdb1a78)        = -1 ENOTTY (Not a tty)
ioctl(1, TIOCGWINSZ, 0xbfdb1a94)        = -1 ENOTTY (Not a tty)
stat64(".", {st_mode=S_IFDIR|0755, st_size=4096, ...}) = 0
open(".", O_RDONLY|O_LARGEFILE|O_CLOEXEC|O_DIRECTORY) = 3
fcntl64(3, F_SETFD, FD_CLOEXEC)         = 0
getdents64(3, /* 21 entries */, 2048)   = 520
lstat64("./sbin", {st_mode=S_IFDIR|0755, st_size=4096, ...}) = 0
lstat64("./srv", {st_mode=S_IFDIR|0755, st_size=4096, ...}) = 0
lstat64("./lib", {st_mode=S_IFDIR|0755, st_size=4096, ...}) = 0
lstat64("./tmp", {st_mode=S_IFDIR|S_ISVTX|0777, st_size=4096, ...}) = 0
lstat64("./run", {st_mode=S_IFDIR|0755, st_size=4096, ...}) = 0
lstat64("./usr", {st_mode=S_IFDIR|0755, st_size=4096, ...}) = 0
lstat64("./etc", {st_mode=S_IFDIR|0755, st_size=4096, ...}) = 0
lstat64("./home", {st_mode=S_IFDIR|0755, st_size=4096, ...}) = 0
lstat64("./root", {st_mode=S_IFDIR|0700, st_size=4096, ...}) = 0
lstat64("./dev", {st_mode=S_IFDIR|0755, st_size=340, ...}) = 0
lstat64("./mnt", {st_mode=S_IFDIR|0755, st_size=4096, ...}) = 0
lstat64("./bin", {st_mode=S_IFDIR|0755, st_size=4096, ...}) = 0
lstat64("./proc", {st_mode=S_IFDIR|0555, st_size=0, ...}) = 0
lstat64("./media", {st_mode=S_IFDIR|0755, st_size=4096, ...}) = 0
lstat64("./sys", {st_mode=S_IFDIR|0555, st_size=0, ...}) = 0
lstat64("./var", {st_mode=S_IFDIR|0755, st_size=4096, ...}) = 0
lstat64("./opt", {st_mode=S_IFDIR|0755, st_size=4096, ...}) = 0
lstat64("./dock", {st_mode=S_IFDIR|0755, st_size=4096, ...}) = 0
getdents64(3, /* 0 entries */, 2048)    = 0
close(3)                                = 0
ioctl(1, TIOCGWINSZ, 0xbfdb19cc)        = -1 ENOTTY (Not a tty)
writev(1, [{iov_base="bin", iov_len=3}bin
dev
dock
etc
home
lib
media
mnt
opt
proc
root
run
sbin
srv
sys
tmp
usr
var
, {iov_base="\n", iov_len=1}], 2) = 4
writev(1, [{iov_base="dev\ndock\netc\nhome\nlib\nmedia\nmnt\n"..., iov_len=75}, {iov_base=NULL, iov_len=0}], 2) = 75
exit_group(0)                           = ?
+++ exited with 0 +++
Removing intermediate container b29b08832aaf
 ---> 559eda076e46
Step 6/8 : RUN echo -e "FROM alpine:latest\n\nRUN uname -r" >> /dock/Dockerfile
 ---> Running in 746337520e16
Removing intermediate container 746337520e16
 ---> cd41cbc782a2
Step 7/8 : RUN echo -e "docker build -t dind /dock\ndocker run -ti -d dind sh" >> /dock/buildsc.sh && chmod +x /dock/buildsc.sh
 ---> Running in 7b41e72a93ad
Removing intermediate container 7b41e72a93ad
 ---> 3d0c6f03eed2
Step 8/8 : ENTRYPOINT /dock/buildsc.sh
 ---> Running in e5c13a74ddfb
Removing intermediate container e5c13a74ddfb
 ---> 85321b941e10
Successfully built 85321b941e10
root@vm1:/home/max/dind#


#Билд контейнера из контейнера:
===============================================
root@vm1:/home/max/dind# docker run --privileged -v /var/run/docker.sock:/var/run/docker.sock -ti 85321b941e10 sh
Sending build context to Docker daemon  3.072kB
Step 1/2 : FROM alpine:latest
 ---> 63081882c6ff
Step 2/2 : RUN uname -r
 ---> Running in 1898c18cc7ee
4.15.0-112-generic
Removing intermediate container 1898c18cc7ee
 ---> fa33e1123f3a
Successfully built fa33e1123f3a
Successfully tagged dind:latest
ede0c7e1141b963b178d5713c2d29902db13c46cecfcaa95f252b2053adda6d3
root@vm1:/home/max/dind# docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
ede0c7e1141b        dind                "sh"                16 seconds ago      Up 16 seconds                           infallible_galois
root@vm1:/home/max/dind#


